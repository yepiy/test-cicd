# Teste de CI e CD

## Meta 1#
* [X] Criar repositṕrio com Git Flow INIT

* [X] Criar Simples API
    * [X] Create;
    * [ ] Read;
    * [ ] Update;
    * [ ] Delete;

---

## Meta 2#

* [X] Teste de git flow :
    * [X] MASTER;
    * [X] DEVELOP;
    * [ ] FEATURE;
    * [ ] HOTFIX;
    * [ ] RELEASE;

---

## Meta 3#

* [ ] Criar CI
* [ ] Criar CD
