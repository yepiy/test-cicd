let express = require('express');
let app = express();
let router = express.Router();
let bodyParser  = require('body-parser');
let homeRoute = require('../routes/home-route');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', homeRoute);

module.exports = app;