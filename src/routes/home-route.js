const express = require('express');
const router  = express.Router();
const homeController = require('../controller/home-controller');

router.get('/(:id)?', homeController.get);
// router.post('/', homeController.post);
// router.put('/:id', homeController.put);
// router.delete('/:id', homeController.delete);

module.exports = router;