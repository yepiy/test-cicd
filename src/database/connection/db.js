let nconf = require('nconf');
let sqlite = require('sqlite');
let bluebird = require('bluebird');
let Promise = bluebird.Promise;

let db = (function () {


    if(this.instance){
        return this.instance;
    }

    this.instance = null;

    let app_path = nconf.get('APP_PATH');
    let database = nconf.get('database');

    let dbPromise = sqlite.open(`${app_path}/${database}`, { Promise });

    this.instance = {
        getConn : () =>{
            return dbPromise;
        }
    };

    return this.instance;

});


module.exports = db;
