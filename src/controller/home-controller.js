let db = require('../database/connection/db');
let bluebird = require('bluebird');
let Promise = bluebird.Promise;

exports.get = async (req, res, next) => {

    try {
        let id = req.params.id;

        let conn = await db().getConn();

        let where = '1 = 1 '

        if(id){
            where += `and id = ${id}`
        }

        const [users] = await Promise.all([
            conn.get(`select * from user where ${where}`)
        ]);

        res.status(200).send({
            users
        });    
        
    } catch (error) {
        next(error);
    }
    
}
