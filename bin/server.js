const nconf = require('nconf');
const fs = require('fs');

// Set up configs
nconf.use('memory');

// First load command line arguments
nconf.argv();
nconf.env();

const NODE_ENV = process.env.NODE_ENV || 'prod';

let APP_PATH = fs.realpathSync(`${__dirname}/../`);
nconf.set('APP_PATH', APP_PATH );

let conf_path = `${APP_PATH}/config`;
nconf.set('CONF_PATH', conf_path );

let file_path_env = `${conf_path}/environments/${NODE_ENV}.json`

if ( ! fs.existsSync(file_path_env) ) {
    console.log('config file not found');
    process.exit(1);
}

nconf.file({ file: file_path_env });

const DOMAIN = nconf.get('DOMAIN') || 'localhost';
const PORT = nconf.get('PORT') || 3000;
const debug = require('debug')('nodestr:server');
const http = require('http');
const express = require('express');
const app = require('../src/api/app');


app.set('port', normalizePort(PORT));
app.set('domain', DOMAIN);

const server = http.createServer(app);

server.listen(PORT, DOMAIN);
server.on('error', onError);
server.on('listening', onListening);


function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        return val;
    }

    if (port >= 0) {
        return port;
    }

    return false;
}



function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }

}

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'pipe ' + addr.port;

    debug('litening on' + bind);
}


console.log(`Server runnig(${NODE_ENV}) : http://${DOMAIN}:${PORT}`);